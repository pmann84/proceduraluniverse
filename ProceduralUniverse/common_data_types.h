#ifndef __COMMON_DATA_TYPES_H__
#define __COMMON_DATA_TYPES_H__

template<typename T>
struct Range
{
   T high;
   T low;
};

struct Colour
{
   int red;
   int green;
   int blue;
};

#endif // __COMMON_DATA_TYPES_H__
