#ifndef __COMMON_UTILS_H__
#define __COMMON_UTILS_H__

#include "common_data_types.h"

namespace CommonUtils
{
   double interpolate_value(double value, Range<double> range, Range<double> new_range);
   Colour interpolate_value(double value, Range<double> range, Range<Colour> colour_range);
}

#endif // __COMMON_UTILS_H__