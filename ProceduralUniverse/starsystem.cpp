#include "starsystem.h"

#include <random>
#include <algorithm>
#include <cmath>
#include <iostream>

StarSystem::StarSystem(double star_temperature)
{
	// -1/log(lifetime)
   // Generate star(s) - expand to include binary, trinary (??)
   m_stars.emplace_back(Star(star_temperature));
   // Generate planet numbers
   const int min_planets = 2;
   //const int max_planets = 10;
   const double max_planets_for_stard = exp(1.75*m_stars[0].Lifetime())+1.0; // (max_planets - min_planets)*exp(-0.000095*m_stars[0].Lifetime()) + min_planets;
   const auto max_planets_for_star = static_cast<int>(max_planets_for_stard); // (int)((max_planets - min_planets)*(-erf((m_stars[0].Lifetime()) / 25000) + min_planets) + min_planets);
   const int num_planets = min_planets + (rand() % static_cast<int>(max_planets_for_star - min_planets + 1));
   const double solar_to_earth_mass = 333054.3; // solar mass (1.989e30) / earth mass (5.972e24)
   // Get total mass available for planets (99.86% of total system mass in host star)
   const double planet_mass_percentage = 0.0014 / 0.9986;
   double total_planetary_mass = m_stars[0].Mass()*planet_mass_percentage*solar_to_earth_mass;
   // Perhaps try a new way of doing this, work out a typical mass distribution over radius from central
   // star, then for each planet, calculate a "probable" radius from previous body (or star), then work
   // out the mass from the distribution
   double total_radius = 0.0;
   for (int i = 0; i < num_planets; ++i)
   {
      // Determine orbital radius delta (i.e. delta from previous planet)
      // Determine planet mass
      // Determine planet radius
      // Calculate Orbital disk that planet occupies
      // Check it doesnt intersect with previous body
      // Create planet!
   }

   // Print details
   std::cout << "STAR SYSTEM INFORMATION: " << std::endl;
   std::cout << "Number of host stars: " << m_stars.size() << std::endl;
   std::cout << "Host Star Information: " << std::endl;
   std::cout << m_stars.front().Description() << std::endl;

   // *** BEGIN INITIAL METHOD ***
   //// Generate random numbers to generate planet masses, generate num_planets random numbers, call them r1..rn.
   //std::vector<double> plan_randoms;
   //std::default_random_engine generator;
   //std::geometric_distribution<int> distribution(0.05);
   //for (int i = 0; i < num_planets; ++i)
   //{
   //   double ran = distribution(generator) + 1;  // ((double)rand() / (RAND_MAX));
   //   plan_randoms.push_back(ran);
   //}
   //// Add all the random numbers up!
   //double sum_rands = 0.0;
   //std::for_each(plan_randoms.begin(), plan_randoms.end(), [&](double n) { sum_rands += n; });
   //// Random fractions total_planetary_mass is (ri / sum) * total_planetary_mass
   //double final_mass = 0.0;
   //for (int i = 0; i < num_planets; ++i)
   //{
   //   double planet_mass = (plan_randoms[i] / sum_rands)*total_planetary_mass;
   //   // Add planet to the list
   //   m_planets.push_back(Planet(planet_mass));
   //   final_mass += planet_mass;
   //}
   // *** END INITIAL METHOD ***
}
