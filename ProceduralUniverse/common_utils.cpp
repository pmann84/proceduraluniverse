#include "common_utils.h"

double CommonUtils::interpolate_value(double value, Range<double> range, Range<double> new_range)
{
   return new_range.low + (((range.high - value) / (range.high - range.low)) * (new_range.high - new_range.low));
}

Colour CommonUtils::interpolate_value(double value, Range<double> range, Range<Colour> colour_range)
{
   const auto red = static_cast<int>(interpolate_value(value, range, Range<double>{ static_cast<double>(colour_range.low.red), static_cast<double>(colour_range.high.red) }));
   const auto green = static_cast<int>(interpolate_value(value, range, Range<double>{ static_cast<double>(colour_range.low.green), static_cast<double>(colour_range.high.green) }));
   const auto blue = static_cast<int>(interpolate_value(value, range, Range<double>{ static_cast<double>(colour_range.low.blue), static_cast<double>(colour_range.high.blue) }));
   return Colour{ red, green, blue };
}