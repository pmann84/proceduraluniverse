#ifndef __ORBIT_H__
#define __ORBIT_H__

class Orbit
{
public:
   Orbit(double orbital_radius);

private:
   double m_orbital_radius;
};

#endif // __ORBIT_H__
