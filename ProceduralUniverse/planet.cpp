#include "planet.h"

Planet::Planet(double mass, double radius, double dist_from_host_star) : m_mass(mass), m_radius(radius)
{
   // Diameter ranges
   // 100 - 1,000km
   // 1,000 - 30,000km
   // 30,000 - 500,000km
   // 500,000 - 50,000,000km
   // 50,000,000 - 120,000,000km

   if (radius < 0.07848) // 1,000km
   {
      m_type = PlanetType::Type::D;
      m_sub_type = PlanetType::SubType::Planet;
      // const Range D_RADIUS_RANGE{ 0.007845, 0.075 }; // 100-1,000km
   }
   else if (radius < 2.3544) // 30,000km
   {
      //const Range A_RADIUS_RANGE{ 0.07845, 0.7845 }; // 1,000-10,000km
      //const Range B_RADIUS_RANGE{ 0.07845, 0.7845 }; // 1,000-10,000km
      //const Range C_RADIUS_RANGE{ 0.07845, 0.7845 }; // 1,000-10,000km
      //const Range X_RADIUS_RANGE{ 0.07845, 0.7845 }; // 1,000-10,000km

      //const Range Q_RADIUS_RANGE{ 0.3138, 1.177 }; // 4,000-15,000km
      //const Range R_RADIUS_RANGE{ 0.3138, 39.240 }; // 4,000-500,000km

      //const Range K_RADIUS_RANGE{ 0.3925, 0.7845 }; // 5,000-10,000km

      //const Range H_RADIUS_RANGE{ 0.628, 1.177 }; // 8,000-15,000km

      //const Range L_RADIUS_RANGE{ 0.7845, 1.177 }; // 10,000-15,000km
      //const Range M_RADIUS_RANGE{ 0.7845, 1.177 }; // 10,000-15,000km
      //const Range N_RADIUS_RANGE{ 0.7845, 1.177 }; // 10,000-15,000km
      //const Range O_RADIUS_RANGE{ 0.7845, 1.177 }; // 10,000-15,000km
      //const Range P_RADIUS_RANGE{ 0.7845, 1.177 }; // 10,000-15,000km
      //const Range Y_RADIUS_RANGE{ 0.7845, 1.177 }; // 10,000-15,000km

      //const Range E_RADIUS_RANGE{ 0.7845, 2.3545 }; // 10,000-30,000km
      //const Range F_RADIUS_RANGE{ 0.7845, 2.3545 }; // 10,000-30,000km
      //const Range G_RADIUS_RANGE{ 0.7845, 2.3545 }; // 10,000-30,000km
   }
   else if (radius < 39.2403) // 500,000km
   {
      // const Range I_RADIUS_RANGE{ 2.3545, 7.848 }; // 30,000-100,000km
      // const Range J_RADIUS_RANGE{ 3.924, 39.240 }; // 50,000-500,000km
   }
   else if (radius < 3924.0307) // 50,000,000km
   {
      // const Range S_RADIUS_RANGE{ 39.240, 3924.031 }; // 500,000-50,000,000km
   }
   else // 50,000,000km+
   {
      // const Range U_RADIUS_RANGE{ 3924.031, 9417.673 }; // 50,000,000-120,000,000km
   }   
}

double Planet::Mass()
{
   return m_mass;
}

double Planet::Radius()
{
   return m_radius;
}
