#include "star.h"

#include <sstream>

#include "common_utils.h"

Star::Star(double temperature) : m_temperature(temperature)
{
   if (temperature < Y_TEMP.low)
   {
      // Too COLD!
      throw;
   }
   else if (temperature < Y_TEMP.high)
   {
      // Brown
      m_spectral_type = SpectralType::Type::Y;
      m_spectral_decimal = calculate_spectral_decimal(temperature, Y_TEMP.low, Y_TEMP.high);
      m_spectral_subtype = SpectralSubType::Type::V;
      m_mass = CommonUtils::interpolate_value(temperature, Y_TEMP, Y_MASS);
      m_radius = CommonUtils::interpolate_value(temperature, Y_TEMP, Y_RADIUS);
      m_colour = CommonUtils::interpolate_value(temperature, Y_TEMP, Y_COLOUR);
   }
   else if (temperature < T_TEMP_HIGH)
   {
      // Red/Purple
      m_spectral_type = SpectralType::Type::T;
      m_spectral_decimal = calculate_spectral_decimal(temperature, T_TEMP_LOW, T_TEMP_HIGH);
      m_spectral_subtype = SpectralSubType::Type::V;
      m_mass = interpolate_value(temperature, T_TEMP_LOW, T_TEMP_HIGH, T_MASS_LOW, T_MASS_HIGH);
      m_radius = interpolate_value(temperature, T_TEMP_LOW, T_TEMP_HIGH, T_RADIUS_LOW, T_RADIUS_HIGH);
      m_colour = interpolate_value(temperature, T_TEMP_LOW, T_TEMP_HIGH, T_COLOUR_LOW, T_COLOUR_HIGH);
   }
   else if (temperature < L_TEMP_HIGH)
   {
      // Very Dark Red
      m_spectral_type = SpectralType::Type::L;
      m_spectral_decimal = calculate_spectral_decimal(temperature, L_TEMP_LOW, L_TEMP_HIGH);
      m_spectral_subtype = SpectralSubType::Type::V;
      m_mass = interpolate_value(temperature, L_TEMP_LOW, L_TEMP_HIGH, L_MASS_LOW, L_MASS_HIGH);
      m_radius = interpolate_value(temperature, L_TEMP_LOW, L_TEMP_HIGH, L_RADIUS_LOW, L_RADIUS_HIGH);
      m_colour = interpolate_value(temperature, L_TEMP_LOW, L_TEMP_HIGH, L_COLOUR_LOW, L_COLOUR_HIGH);
   }
   else if (temperature < M_TEMP_HIGH)
   {
      // Red
      m_spectral_type = SpectralType::Type::M;
      m_spectral_decimal = calculate_spectral_decimal(temperature, M_TEMP_LOW, M_TEMP_HIGH);
      m_spectral_subtype = SpectralSubType::Type::V;
      m_mass = interpolate_value(temperature, M_TEMP_LOW, M_TEMP_HIGH, M_MASS_LOW, M_MASS_HIGH);
      m_radius = interpolate_value(temperature, M_TEMP_LOW, M_TEMP_HIGH, M_RADIUS_LOW, M_RADIUS_HIGH);
      m_colour = interpolate_value(temperature, M_TEMP_LOW, M_TEMP_HIGH, M_COLOUR_LOW, M_COLOUR_HIGH);
   }
   else if (temperature < K_TEMP_HIGH)
   {
      // Orange -> Red
      m_spectral_type = SpectralType::Type::K;
      m_spectral_decimal = calculate_spectral_decimal(temperature, K_TEMP_LOW, K_TEMP_HIGH);
      m_spectral_subtype = SpectralSubType::Type::V;
      m_mass = interpolate_value(temperature, K_TEMP_LOW, K_TEMP_HIGH, K_MASS_LOW, K_MASS_HIGH);
      m_radius = interpolate_value(temperature, K_TEMP_LOW, K_TEMP_HIGH, K_RADIUS_LOW, K_RADIUS_HIGH);
      m_colour = interpolate_value(temperature, K_TEMP_LOW, K_TEMP_HIGH, K_COLOUR_LOW, K_COLOUR_HIGH);
   }
   else if (temperature < G_TEMP_HIGH)
   {
      // White -> Yellow
      m_spectral_type = SpectralType::Type::G;
      m_spectral_decimal = calculate_spectral_decimal(temperature, G_TEMP_LOW, G_TEMP_HIGH);
      m_spectral_subtype = SpectralSubType::Type::V;
      m_mass = interpolate_value(temperature, G_TEMP_LOW, G_TEMP_HIGH, G_MASS_LOW, G_MASS_HIGH);
      m_radius = interpolate_value(temperature, G_TEMP_LOW, G_TEMP_HIGH, G_RADIUS_LOW, G_RADIUS_HIGH);
      m_colour = interpolate_value(temperature, G_TEMP_LOW, G_TEMP_HIGH, G_COLOUR_LOW, G_COLOUR_HIGH);
   }
   else if (temperature < F_TEMP_HIGH)
   {
      // Blue -> White
      m_spectral_type = SpectralType::Type::F;
      m_spectral_decimal = calculate_spectral_decimal(temperature, F_TEMP_LOW, F_TEMP_HIGH);
      m_spectral_subtype = SpectralSubType::Type::V;
      m_mass = interpolate_value(temperature, F_TEMP_LOW, F_TEMP_HIGH, F_MASS_LOW, F_MASS_HIGH);
      m_radius = interpolate_value(temperature, F_TEMP_LOW, F_TEMP_HIGH, F_RADIUS_LOW, F_RADIUS_HIGH);
      m_colour = interpolate_value(temperature, F_TEMP_LOW, F_TEMP_HIGH, F_COLOUR_LOW, F_COLOUR_HIGH);
   }
   else if (temperature < A_TEMP_HIGH)
   {
      // Blue
      m_spectral_type = SpectralType::Type::A;
      m_spectral_decimal = calculate_spectral_decimal(temperature, A_TEMP_LOW, A_TEMP_HIGH);
      m_spectral_subtype = SpectralSubType::Type::V;
      m_mass = interpolate_value(temperature, A_TEMP_LOW, A_TEMP_HIGH, A_MASS_LOW, A_MASS_HIGH);
      m_radius = interpolate_value(temperature, A_TEMP_LOW, A_TEMP_HIGH, A_RADIUS_LOW, A_RADIUS_HIGH);
      m_colour = interpolate_value(temperature, A_TEMP_LOW, A_TEMP_HIGH, A_COLOUR_LOW, A_COLOUR_HIGH);
   }
   else if (temperature < B_TEMP_HIGH)
   {
      // Blue
      m_spectral_type = SpectralType::Type::B;
      m_spectral_decimal = calculate_spectral_decimal(temperature, B_TEMP_LOW, B_TEMP_HIGH);
      m_spectral_subtype = SpectralSubType::Type::V;
      m_mass = interpolate_value(temperature, B_TEMP_LOW, B_TEMP_HIGH, B_MASS_LOW, B_MASS_HIGH);
      m_radius = interpolate_value(temperature, B_TEMP_LOW, B_TEMP_HIGH, B_RADIUS_LOW, B_RADIUS_HIGH);
      m_colour = interpolate_value(temperature, B_TEMP_LOW, B_TEMP_HIGH, B_COLOUR_LOW, B_COLOUR_HIGH);
   }
   else
   {
      // Blue
      m_spectral_type = SpectralType::Type::O;
      m_spectral_decimal = calculate_spectral_decimal(temperature, O_TEMP_LOW, O_TEMP_HIGH);
      m_spectral_subtype = SpectralSubType::Type::V;
      m_mass = interpolate_value(temperature, O_TEMP_LOW, O_TEMP_HIGH, O_MASS_LOW, O_MASS_HIGH);
      m_radius = interpolate_value(temperature, O_TEMP_LOW, O_TEMP_HIGH, O_RADIUS_LOW, O_RADIUS_HIGH);
      m_colour = interpolate_value(temperature, O_TEMP_LOW, O_TEMP_HIGH, O_COLOUR_LOW, O_COLOUR_HIGH);
   }
   m_metallicity = METALLICITY_MULTIPLIER / m_temperature;
   m_lifetime = 1.0 / std::pow(m_mass, 2.5);
}

double Star::Mass() const
{
   return m_mass;
}

double Star::Lifetime() const
{
	return m_lifetime;
}

std::string Star::Description() const
{
   std::stringstream ss;
   ss << "Spectral Type: " << SpectralType::Parse(m_spectral_type) << m_spectral_decimal << SpectralSubType::Parse(m_spectral_subtype) << std::endl;
   ss << "Surface Temperature: " << m_temperature << "K" << std::endl;
   ss << "Mass: " << m_mass * SOLAR_MASS << "kg" << std::endl;
   ss << "Radius: " << m_radius * SOLAR_RADIUS << "km" << std::endl;
   ss << "Metallicity: " << m_metallicity * 100 << "%" << std::endl;
   return ss.str();
}

Colour Star::interpolate_value(double temperature, double temp_low, double temp_high, Colour new_low, Colour new_high)
{
   const auto red = static_cast<int>(interpolate_value(temperature, temp_low, temp_high, new_low.red, new_high.red));
   const auto green = static_cast<int>(interpolate_value(temperature, temp_low, temp_high, new_low.green, new_high.green));
   const auto blue = static_cast<int>(interpolate_value(temperature, temp_low, temp_high, new_low.blue, new_high.blue));
   return Colour{ red, green, blue };
}

double Star::interpolate_value(double temperature, double temp_low, double temp_high, double new_low, double new_high)
{
   return new_low + (((temp_high - temperature) / (temp_high - temp_low)) * (new_high - new_low));
}

int Star::calculate_spectral_decimal(double temperature, double temp_low, double temp_high)
{
   return static_cast<int>(std::floor(((temp_high - temperature) / (temp_high - temp_low)) * 10.0));
}
