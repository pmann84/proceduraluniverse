#ifndef __STAR_H__
#define __STAR_H__

#include <string>
#include <tuple>
#include "common_data_types.h"

struct SpectralType
{
   // Spectral enum types
   enum class Type
   {
      Y, // Brown (250-500K)
      T, // Red (500-1,300K)
      L, // Red (1,300-2,400K)
      M, // Red (2,400->3,700K)
      K, // Orange -> Red (3,700-5,200K)
      G, // White -> Yellow (5,200-6,000K)
      F, // Blue -> White (6,000-7,500K)
      A, // Blue (7,500-10,000K)
      B, // Blue (10,000-30,000K)
      O // Blue (30,000K+)
   };

   static std::string Parse(Type spectral_type)
   {
      switch (spectral_type)
      {
      case Type::Y:
         return "Y";
      case Type::T:
         return "T";
      case Type::L:
         return "L";
      case Type::M:
         return "M";
      case Type::K:
         return "K";
      case Type::G:
         return "G";
      case Type::F:
         return "F";
      case Type::A:
         return "A";
      case Type::B:
         return "B";
      case Type::O:
         return "O";
      default:
         throw;
      }
   }
};

struct SpectralSubType
{
   enum class Type
   {
      Ia, // Very luminous supergiants
      Ib, // Less luminous supergiants
      II, // Luminous giants
      III, // Giants
      IV, // Subgiants
      V, // Main sequence stars(dwarf stars)
      VI, // Subdwarf
      VII // White Dwarf
   };

   static std::string Parse(Type spectral_sub_type)
   {
      switch (spectral_sub_type)
      {
      case Type::Ia:
         return "Ia";
      case Type::Ib:
         return "Ib";
      case Type::II:
         return "II";
      case Type::III:
         return "III";
      case Type::IV:
         return "IV";
      case Type::V:
         return "V";
      case Type::VI:
         return "VI";
      case Type::VII:
         return "VII";
      default:
         throw;
      }
   }
};

// Spectral Type Y constants
//const double Y_TEMP_LOW = 250.0;
//const double Y_TEMP_HIGH = 500.0;
const Range<double> Y_TEMP = Range<double>{ 250.0, 500.0 };
//const double Y_MASS_LOW = 0.003;
//const double Y_MASS_HIGH = 0.005;
const Range<double> Y_MASS = Range<double>{ 0.003, 0.005 };
//const double Y_RADIUS_LOW = 0.004; 
//const double Y_RADIUS_HIGH = 0.005;
const Range<double> Y_RADIUS = Range<double>{ 0.004, 0.005 };
//const Colour Y_COLOUR_LOW = Colour{ 153, 51, 51 };
//const Colour Y_COLOUR_HIGH = Colour{ 161, 2, 52 };
const Range<Colour> Y_COLOUR = Range<Colour>{ Colour{ 153, 51, 51 }, Colour{ 161, 2, 52 } };


// Spectral Type T constants
const double T_TEMP_LOW = 500.0;
const double T_TEMP_HIGH = 1300.0;
const double T_MASS_LOW = 0.005;
const double T_MASS_HIGH = 0.01;
const double T_RADIUS_LOW = 0.005;
const double T_RADIUS_HIGH = 0.01;
const Colour T_COLOUR_LOW = Colour{ 161, 2, 52 };
const Colour T_COLOUR_HIGH = Colour{ 125, 0, 0 };

// Spectral Type L constants
const double L_TEMP_LOW = 1300.0;
const double L_TEMP_HIGH = 2400.0;
const double L_MASS_LOW = 0.01;
const double L_MASS_HIGH = 0.08;
const double L_RADIUS_LOW = 0.01;
const double L_RADIUS_HIGH = 0.08;
const Colour L_COLOUR_LOW = Colour{ 125, 0, 0 };
const Colour L_COLOUR_HIGH = Colour{ 255, 0, 0 };

// Spectral Type M constants
const double M_TEMP_LOW = 2400.0;
const double M_TEMP_HIGH = 3700.0;
const double M_MASS_LOW = 0.08;
const double M_MASS_HIGH = 0.45;
const double M_RADIUS_LOW = 0.08;
const double M_RADIUS_HIGH = 0.7;
const Colour M_COLOUR_LOW = Colour{ 255, 0, 0};
const Colour M_COLOUR_HIGH = Colour{ 255, 102, 0 };

// Spectral Type K constants
const double K_TEMP_LOW = 3700.0;
const double K_TEMP_HIGH = 5200.0;
const double K_MASS_LOW = 0.45;
const double K_MASS_HIGH = 0.8;
const double K_RADIUS_LOW = 0.7;
const double K_RADIUS_HIGH = 0.96;
const Colour K_COLOUR_LOW = Colour{ 255, 102, 0 };
const Colour K_COLOUR_HIGH = Colour{ 255, 255, 0 };

// Spectral Type G constants
const double G_TEMP_LOW = 5200.0;
const double G_TEMP_HIGH = 6000.0;
const double G_MASS_LOW = 0.8;
const double G_MASS_HIGH = 1.04;
const double G_RADIUS_LOW = 0.96;
const double G_RADIUS_HIGH = 1.15;
const Colour G_COLOUR_LOW = Colour{ 255, 255, 0 };
const Colour G_COLOUR_HIGH = Colour{ 255, 255, 255 };

// Spectral Type F constants
const double F_TEMP_LOW = 6000.0;
const double F_TEMP_HIGH = 7500.0;
const double F_MASS_LOW = 1.04;
const double F_MASS_HIGH = 1.4;
const double F_RADIUS_LOW = 1.15;
const double F_RADIUS_HIGH = 1.4;
const Colour F_COLOUR_LOW = Colour{ 255, 255, 255 };
const Colour F_COLOUR_HIGH = Colour{ 198, 195, 255 };

// Spectral Type A constants
const double A_TEMP_LOW = 7500.0;
const double A_TEMP_HIGH = 10000.0;
const double A_MASS_LOW = 1.4;
const double A_MASS_HIGH = 2.1;
const double A_RADIUS_LOW = 1.4;
const double A_RADIUS_HIGH = 1.8;
const Colour A_COLOUR_LOW = Colour{ 198, 195, 255 };
const Colour A_COLOUR_HIGH = Colour{ 136, 130, 255 };

// Spectral Type B constants
const double B_TEMP_LOW = 10000.0;
const double B_TEMP_HIGH = 30000.0;
const double B_MASS_LOW = 2.1;
const double B_MASS_HIGH = 16.0;
const double B_RADIUS_LOW = 1.8;
const double B_RADIUS_HIGH = 6.6;
const Colour B_COLOUR_LOW = Colour{ 136, 130, 255 };
const Colour B_COLOUR_HIGH = Colour{ 74, 65, 255 };

// Spectral Type O constants
const double O_TEMP_LOW = 30000.0;
const double O_TEMP_HIGH = 80000.0;
const double O_MASS_LOW = 16.0;
const double O_MASS_HIGH = 20.0;
const double O_RADIUS_LOW = 6.6;
const double O_RADIUS_HIGH = 8.0;
const Colour O_COLOUR_LOW = Colour{ 74, 65, 255 };
const Colour O_COLOUR_HIGH = Colour{ 13, 0, 255 };

// General Constants
const double METALLICITY_MULTIPLIER = 77.318;
const long double SOLAR_MASS = 2e30; 
const double SOLAR_RADIUS = 6.95508e5;

// Colour spectrum following the following colours
// 993333 -> rgb(153,51,51) -> Brown
// A10234 -> rgb(161,2,52) -> Red/Purple
// 7D0000 -> rgb(125,0,0) -> Dark Red
// FF0000 -> rgb(255,0,0) -> Red
// FF6600 -> rgb(255,102,0) -> Orange
// FFE100 -> rgb(255,225,0) -> Yellow
// FFFFFF -> rgb(255,255,255) -> White
// C6C3FF -> rgb(198,195,255) -> Blue
// 8882FF -> rgb(136,130,255) -> Blue
// 4A41FF -> rgb(74,65,255) -> Blue
// 0D00FF -> rgb(13,0,255) -> Blue

// Lifetime = Mass / Luminosity (fuel consumption rate) = 1 / M^3
// Metallicity => 1 / Teff (not rigourous - a starting point for this)
// - Often compared as [Fe/H] but more generally Z = 1 - X - Y (X = hydrogen fractional %, Y = helium fractional %)
// Red stars have higher metallicity and blue stars lower metallicity, so 
// higher temperature stars have lower metallicity which supports the above relation
// Solar value is Z = 0.0134, crude interpolation gives:
// Z = K / T (K = constant = 77.318)
// Max Z = K / 80000 = 0.000966
// Min Z = K / 500 = 0.154636

class Star
{
public:
   Star(double temperature);

   double Mass() const;
   double Lifetime() const;
   std::string Description() const;

private:
   Colour interpolate_value(double temperature, double temp_low, double temp_high, Colour new_low, Colour new_high);
   double interpolate_value(double temperature, double temp_low, double temp_high, double new_low, double new_high);
   int calculate_spectral_decimal(double temperature, double temp_low, double temp_high);

   double m_temperature;
   double m_mass; // Solar masses
   double m_lifetime;
   double m_radius; // Solar Radius
   double m_metallicity; // decimal value of % i.e. max value of 1
   SpectralType::Type m_spectral_type;
   int m_spectral_decimal;
   SpectralSubType::Type m_spectral_subtype;
   Colour m_colour;
};

#endif // __STAR_H__