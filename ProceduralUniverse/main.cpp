#include "starsystem.h"

// TODO: Add support for non-main-sequence stars (add in luminosity parameter to give ability to create other type stars)
// TODO: Add calculation of metalicity
// TODO: Add support for Binary and trinary systems
// TODO: Make calculation of planet numbers more scientific (i.e. not random and update relationship between lifetime and number of planets)
// TODO: Make calculation of planet masses more scientific (i.e. not fixed to 99.9%)
// TODO: Need to add seed, to not make it random on regeneration
// TODO: Update star system to be able to generate binary, trinary (?) systems
// TODO: Update star systems to generate asteroid belts
// TODO: Support other orbit types - extend orbit class to essentially be a conic section
// TODO: Improve Metallicity calculation (?? - could be sufficient for this)
// TODO: Support stellar remnants e.g. black holes, neutron stars, pulsars, quasars etc
// TODO: Add rings to certain planet types
// TODO: Nebulae?????

// INFO: For a galactic disk, stars in the halo have high metallicity i.e. red stars, and in the disk lower metallicity i.e. blue stars


// Star and planet classifications - could use this : http://sttff.net/planetaryclass.html
int main()
{
   // Star system level - 
   // star level generation - number of stars, type of stars
   //       - input: temperature, luminosity
   // planet level (including belts etc)
   // satellite level 
   StarSystem ss(5770.0);
   return 0;
}