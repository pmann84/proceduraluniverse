﻿#ifndef __STAR_SYSTEM_H__
#define __STAR_SYSTEM_H__

#include <vector>
#include <map>

#include "star.h"
#include "planet.h"
#include "orbit.h"

/*At the low-mass end of star-formation are sub-stellar objects that don't fuse hydrogen: the brown dwarfs and sub-brown dwarfs, 
  of spectral classification L,T and Y. Planets and protoplanetary disks have been discovered around brown dwarfs, and disks 
  have been found around sub-brown dwarfs (e.g. OTS 44). Ordinary stars are composed mainly of the light elements hydrogen and 
  helium. They also contain a small proportion of heavier elements, and this fraction is referred to as a star's metallicity 
  (even if the elements are not metals in the traditional sense) denoted [m/H] and expressed on a logarithmic scale where zero 
  is the Sun's metallicity. A 2012 study of the Kepler spacecraft data found that smaller planets, with radii smaller than 
  Neptune's were found around stars with metallicities in the range −0.6 < [m/H] < +0.5 (about four times less than that of the 
  Sun to three times more), whereas larger planets were found mostly around stars with metallicities at the higher end of this 
  range (at solar metallicity and above). 
  In this study small planets occurred about three times as frequently as large planets around stars of metallicity greater than that of the Sun, 
  but they occurred around six times as frequently for stars of metallicity less than that of the Sun. The lack of gas giants around low-metallicity stars 
  could be because the metallicity of protoplanetary disks affects how quickly planetary cores can form and whether they accrete a gaseous envelope before 
  the gas dissipates. However, Kepler can only observe planets very close to their star and the detected gas giants probably migrated from further out, 
  so a decreased efficiency of migration in low-metallicity disks could also partly explain these findings.[27] A 2014 study found that not only giant planets, 
  but planets of all sizes, have an increased occurrence rate around metal-rich stars compared to metal-poor stars, although the larger the planet, the greater 
  this increase as the metallicity increases. The study divided planets into three groups based on radius: gas giants, gas dwarfs, and terrestrial planets with 
  the dividing lines at 1.7 and 3.9 Earth radii. For these three groups, the planet occurrence rates are 9.30, 2.03, and 1.72 times higher for metal-rich stars 
  than for metal-poor stars, respectively. There is a bias against detecting smaller planets because metal-rich stars tend to be larger, making it more difficult 
  to detect smaller planets, which means that these increases in occurrence rates are lower limits. It has also been shown that stars with planets are more likely 
  to be deficient in lithium.*/

class StarSystem
{
public:
   StarSystem(double star_temperature);

private:
   std::vector<Star> m_stars;
   std::vector<Planet> m_planets;
   std::map<Planet, Orbit> m_planetary_orbits;
};

#endif // __STAR_SYSTEM_H__