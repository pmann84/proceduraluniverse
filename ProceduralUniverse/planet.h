#ifndef __PLANET_H__
#define __PLANET_H__

#include <string>
#include <tuple>
#include "common_data_types.h"

struct PlanetType
{
   enum class Type
   {
      A, B, C, D, E, F,
      G, H, I, J, K, L,
      M, N, O, P, Q, R,
      S, U, X, Y
   };

   enum class SubType
   {
      Planet,
      Satellite
   };

   static std::tuple<std::string, std::string> Parse(Type planet_type)
   {
      switch (planet_type)
      {
      case Type::A:
         return std::make_tuple("A", "Geothermal");
      case Type::B:
         return std::make_tuple("B", "Geomorteus");
      case Type::C:
         return std::make_tuple("C", "Geoinactive");
      case Type::D:
         return std::make_tuple("D", "Dwarf");
      case Type::E:
         return std::make_tuple("E", "Geoplastic");
      case Type::F:
         return std::make_tuple("F", "Geometallic");
      case Type::G:
         return std::make_tuple("G", "Geocrystalline");
      case Type::H:
         return std::make_tuple("H", "Desert");
      case Type::I:
         return std::make_tuple("I", "Ice Giant");
      case Type::J:
         return std::make_tuple("J", "Jovian");
      case Type::K:
         return std::make_tuple("K", "Adaptable");
      case Type::L:
         return std::make_tuple("L", "Marginal");
      case Type::M:
         return std::make_tuple("M", "Terrestrial");
      case Type::N:
         return std::make_tuple("N", "Reducing");
      case Type::O:
         return std::make_tuple("O", "Pelagic");
      case Type::P:
         return std::make_tuple("P", "Glaciated");
      case Type::Q:
         return std::make_tuple("Q", "Variable");
      case Type::R:
         return std::make_tuple("R", "Rogue");
      case Type::S:
         return std::make_tuple("S", "Super Giant");
      case Type::U:
         return std::make_tuple("U", "Ultra Giant");
      case Type::X:
         return std::make_tuple("X", "Chthonian");
      case Type::Y:
         return std::make_tuple("Y", "Demon");
      default:
         throw;
      }
   }
};

const double EARTH_RADIUS = 6.371e3; // km

// NOTE: Ranges in comments are diameters!
const Range<double> D_RADIUS_RANGE{ 0.007845, 0.075 }; // 100-1,000km

const Range<double> A_RADIUS_RANGE{ 0.07845, 0.7845 }; // 1,000-10,000km
const Range<double> B_RADIUS_RANGE{ 0.07845, 0.7845 }; // 1,000-10,000km
const Range<double> C_RADIUS_RANGE{ 0.07845, 0.7845 }; // 1,000-10,000km
const Range<double> X_RADIUS_RANGE{ 0.07845, 0.7845 }; // 1,000-10,000km

const Range<double> Q_RADIUS_RANGE{ 0.3138, 1.177 }; // 4,000-15,000km
const Range<double> R_RADIUS_RANGE{ 0.3138, 39.240 }; // 4,000-500,000km

const Range<double> K_RADIUS_RANGE{ 0.3925, 0.7845 }; // 5,000-10,000km

const Range<double> H_RADIUS_RANGE{ 0.628, 1.177 }; // 8,000-15,000km

const Range<double> L_RADIUS_RANGE{ 0.7845, 1.177 }; // 10,000-15,000km
const Range<double> M_RADIUS_RANGE{ 0.7845, 1.177 }; // 10,000-15,000km
const Range<double> N_RADIUS_RANGE{ 0.7845, 1.177 }; // 10,000-15,000km
const Range<double> O_RADIUS_RANGE{ 0.7845, 1.177 }; // 10,000-15,000km
const Range<double> P_RADIUS_RANGE{ 0.7845, 1.177 }; // 10,000-15,000km
const Range<double> Y_RADIUS_RANGE{ 0.7845, 1.177 }; // 10,000-15,000km

const Range<double> E_RADIUS_RANGE{ 0.7845, 2.3545 }; // 10,000-30,000km
const Range<double> F_RADIUS_RANGE{ 0.7845, 2.3545 }; // 10,000-30,000km
const Range<double> G_RADIUS_RANGE{ 0.7845, 2.3545 }; // 10,000-30,000km

const Range<double> I_RADIUS_RANGE{ 2.3545, 7.848 }; // 30,000-100,000km

const Range<double> J_RADIUS_RANGE{ 3.924, 39.240 }; // 50,000-500,000km

const Range<double> S_RADIUS_RANGE{ 39.240, 3924.031 }; // 500,000-50,000,000km
const Range<double> U_RADIUS_RANGE{ 3924.031, 9417.673 }; // 50,000,000-120,000,000km


class Planet
{
public:
   Planet(double mass, double radius, double dist_from_host_star);

   double Mass();
   double Radius();

private:
   PlanetType::Type m_type;
   PlanetType::SubType m_sub_type;
   double m_mass; // Earth masses
   double m_radius; // Earth radii
};

#endif // __PLANET_H__